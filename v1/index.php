<?php
	session_start();
	date_default_timezone_set('Brazil/East');
	require_once "php/servicos/RegistraContagemAcessos.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>yobib.</title>
	<link rel="shortcut icon" href="images/icone_yobib.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="resources/porgressbar/progress.css" rel="stylesheet" type="text/css" />
	<link href="resources/tooltipser/tooltipster.bundle.css" rel="stylesheet" type="text/css" />
	<link href="resources/tooltipser/tooltipster-sideTip-borderless.min.css" rel="stylesheet" type="text/css" />
	
	<!-- build:css css/index.min.css -->
	<link href="css/index.css" rel="stylesheet" type="text/css" />
	<!-- endbuild -->

	<meta name="application-name" content="Yobib"/> 
	<meta name="autor" content="Adriano Marques"/> 
	<meta name="description" content="" />
	<meta name="keywords" content="yobib, Yobib, YOBIB, upload de arquivos, cloud, arquivos, armazenamento, upload, transferência, download, drive, nuvem, arquivos em nuvem" />
</head>
<body>
	<main>
		<header id="cabecalho">
			<nav>
				<ul>
					<li><a href="."><img src="images/logo_yobib.svg" alt="yobib"></a></li>
				</ul>
			</nav>
		</header>

		<article id="central">
			<?php 
				if (isset($_SESSION["arquivo_removido"])) {
					echo "<h1>Seu arquivo foi removido :(</h1>";
					unset($_SESSION["arquivo_removido"]);
					die();
				}
			?>

			<section id="principal">
				<div>
					<form method="post" action="php/servicos/RealizaUpload.php" name="form-upload" id="form-upload" class="formulario" enctype="multipart/form-data" autocomplete="off">
						<div class="controls-form tooltip-formulario" title="Clique aqui para adicionar arquivos">
							<span id="ajudaClick">Como funciona <img src="images/interogation.svg" alt="Como funciona?"></span>
							<label>Fazer upload de</label>
							<ul>
								<li>
									<div class="botao-upload" id="botao-arquivo">
										<img src="images/upload-arquivo.svg" alt="Upload de arquivos">
						    			<span>Arquivo</span>
						    			<input type="file" name="arquivos[]" multiple="multiple" class="input-upload" id="arquivos" required="required">
									</div>
								</li>
								<li>
									<div class="botao-upload" id="botao-pasta">
										<img src="images/upload-pasta.svg" alt="Upload de pasta">
						    			<span>Pasta</span>
						    			<input type="file" name="pasta[]" multiple="multiple" class="input-upload" id="pasta" required="required" webkitdirectory directory>
									</div>
								</li>
							</ul>

							<span id="arquivosUpload"></span>
						</div>

						<div class="controls-form tooltip-formulario" title="Adicione seu próprio endereço de e-mail aqui">
							<input type="email" class="campo-form" name="email" id="email" placeholder="O seu e-mail">
						</div>

						<div class="controls-form tooltip-formulario" id="lista-email-amigos" title="Adicione o endereço de e-mail de seus amigos que vão receber o arquivo">
							<input type="email" name="emails-amigos[]" class="campo-form email-amigo" id="mail-friend" placeholder="E-mail do amigo">
							<img src="images/add-email.svg" alt="Adicionar e-mail do amigo" id="add-mail-friend" title="Adicione mais e-mails de amigos">
						</div>

						<div class="controls-form tooltip-formulario" title="Adicione uma mensagem para enviar aos seus amigos" id="box-mensagem">
							<textarea name="mensagem" class="campo-form" placeholder="Mensagem" id="mensagem"></textarea>
						</div>

						<input type="submit" name="btn-upload" id="btn-upload" class="botao tooltip-formulario" title="Clique aqui para compartilhar!" value="Upload!">
					</form>
				</div>

				<div>
					<h1>Tudo para todos.<br>Compartilhe!</h1>	
				</div>
			</section>
		</article>

		<section id="loading">
			<section id="panel-loading">
				<header>
					<h1 id="status-upload"><span id="close-upload">X</span></h1>
				</header>
				<div class="pie_progress" role="progressbar" data-goal="100" aria-valuemin="0" aria-valuemax="100">
		            <span class="pie_progress__number">0%</span>
		        </div>
		        <article id="compartilhar">
		        	<!--Gerado no js-->
		        </article>
			</section>
		</section>

		<section id="notification"></section>
		<section id="ajuda"></section>

		<footer>
			<div id="copy">&copy<?php echo date("Y")?> - yobib. Todos os direitos reservados.</div>
			<div id="developer">Desenvolvido por <a href="https://www.linkedin.com/in/adriano-marques-04b522b6" target="_blank">Adriano Marques.</a></div>
		</footer>
	</main>
	<script src="resources/jquery/jquery-2.2.3.min.js"></script>
	<script src="resources/jquery/jquery.easing.min.js"></script>
	<script src="resources/porgressbar/jquery-asPieProgress.js"></script>
	<script src="resources/jquery/jquery.form.js"></script>
	<script src="resources/tooltipser/tooltipster.bundle.js"></script>
	<script src="js/index.js"></script>
	<script src="js/formulario.js"></script>
	<!-- build:js js/upload.min.js -->
	<script src="js/upload.js"></script>
	<!-- endbuild -->
</body>
</html>