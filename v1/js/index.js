function formatFileSize(bytes, decimals) {
   if(bytes == 0) return '0 bytes';
   var k = 1000;
   var dm = decimals + 1 || 3;
   var sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

var notificacao = function(texto = null){
    $("#notification").text(texto);
    $("#notification").fadeIn("fast");

    setTimeout(function(){
        $("#notification").fadeOut("fast");
    }, 2000);
}

var uploadComplete = function() {
    $("#status-upload").html("Upload completo! <span id='close-upload'>X</span>");
    $("#compartilhar").fadeIn("fast");
    $("#arquivos-success").fadeIn("fast");
    $("#close-upload").fadeIn("fast");

    $('.tooltip').tooltipster({
        side: 'right',
        theme: 'tooltipster-borderless',
        delay: 0,
        animation: 'fade'
    });
};

var listaArquivos = function(){
    var lista = new Array();

    var arquivos = $("#arquivos")[0].files;
    for (var i = 0; i < arquivos.length; i++) {
        lista.push(arquivos[i]);
    }

    var arquivosPasta = $("#pasta")[0].files;
    for (var i = 0; i < arquivosPasta.length; i++) {
        lista.push(arquivosPasta[i]);
    }

    return lista;
}

$(function(){
    var stateTooltipser = "close";
    var funcClickActive = false;
    $(document).keyup(function(e) {
        if (e.keyCode == 27 && funcClickActive == true) {
            funcClickActive = false;
            stateTooltipser = "close";
            $('#ajuda').fadeOut("fast");
            $('.tooltip-formulario').tooltipster('close');
        }
        
    });

    $(document).click(function(){
        if(funcClickActive == true){
            funcClickActive = false;
            stateTooltipser = "close";
            $('#ajuda').fadeOut("fast");
            $('.tooltip-formulario').tooltipster('close');
        }
    });

    $("#ajudaClick").click(function(){
        if(stateTooltipser == "close"){
            stateTooltipser = "open";
            $('#ajuda').fadeIn("fast");
            $('.tooltip-formulario').tooltipster('open');

            setTimeout(function(){
                funcClickActive = true;    
            }, 250);
        } else {
            stateTooltipser = "close";
            $('#ajuda').fadeOut("fast");
            $('.tooltip-formulario').tooltipster('close');
            setTimeout(function(){
                funcClickActive = false;
            }, 250);
        }
    })

	$('.pie_progress').asPieProgress({
        namespace: 'pie_progress'
    });

    $('.tooltip-formulario').tooltipster({
        side: 'right',
        theme: 'tooltipster-borderless',
        delay: 0,
        animation: 'grow',
        contentAsHTML: true,
        trigger: "none"
    });

    $('#add-mail-friend').tooltipster({
        side: 'right',
        theme: 'tooltipster-borderless',
        delay: 0,
        animation: 'fade',
        contentAsHTML: true
    });

    $("#loading").delegate("#close-upload" ,"click", function() {
        location.reload();
    });

    $("#loading").delegate("#btn-copiar-link" ,"click", function() {
        var copyTextarea = document.getElementById("link-compartilhar");
        copyTextarea.select();

        try {
            var successful = document.execCommand('copy');
            if(successful){
                notificacao("Link copiado!");
            }           
        } catch (err) {
            
        }
    });

    // Download
    $("#arquivos-uploaded tbody tr").click(function(){
        $(this).toggleClass("selecionada");
    })
    
    $("#select-all").click(function(){
        if($(this).html() == 'Marcar todos <img src="../../images/check.svg">'){
            $(this).html('Desmarcar todos <img src="../../images/check.svg">');
            $("#arquivos-uploaded tbody tr").addClass("selecionada");
        } else {
            $("#arquivos-uploaded tbody tr").removeClass("selecionada");
            $(this).html('Marcar todos <img src="../../images/check.svg">');
        }
    })

    $("#download").click(function(){
        var arquivosSelecionados = $("#arquivos-uploaded tbody tr.selecionada");
        if (arquivosSelecionados.length > 0) {
            $(this).removeClass("tooltip2");
            $(this).removeAttr('title');

            for (var i = 0; i < arquivosSelecionados.length; i++) {
                var idDownload = $(arquivosSelecionados[i]).attr('id') + "download";
                document.getElementById(idDownload).click();
            }
        } else {
            $(this).attr('title', "Selecione um arquivo para download");
            $(this).addClass("tooltip2");

            $('.tooltip2').tooltipster({
                side: 'top',
                theme: 'tooltipster-borderless',
                delay: 0,
                animation: 'fade'
            });
            $('.tooltip2').tooltipster("open");
        }
    });
});