$(function(){
	$("#pasta").change(function() {
		$("#arquivos").removeAttr('required');
	});

	$("#arquivos").change(function() {
		$("#pasta").removeAttr('required');
	});

	$("#email").focusout(function() {
		if($(this).val() != ""){
			$("#mail-friend").attr("required", "required");
		} else {
			$("#mail-friend").removeAttr('required');
		}
	});

	$("#mail-friend").focusout(function() {
		if($(this).val() != ""){
			$("#email").attr("required", "required");
		} else {
			$("#email").removeAttr('required');
		}
	});

	    //change nos arquivos
    $(".input-upload").change(function(){
        var arquivos = listaArquivos();
        var plural = (arquivos.length == 1) ? arquivos.length + " arquivo" : arquivos.length + " arquivos";
        $("#status-upload").html("Fazendo upload de " + plural + "...");

        var plural2 = (arquivos.length == 1) ? " selecionado..." : " selecionados...";
        $("#status-upload").html("Fazendo upload de " + plural + "...");

        $("#arquivosUpload").html(plural + plural2);
        $("#arquivosUpload").fadeIn("fast");
        $("#arquivosUpload").css({
            "display": "inline-block"
        });

        $("#add-mail-friend").css({
            "top": "217px"
        });

        $(".formulario").css({
            "height": "430px"
        });

        var tableArquivos = "<table>";
        for (var i = 0; i < arquivos.length; i++) {
            var nomeArquivo = arquivos[i].name;
            if (arquivos[i].name.length > 25) {
                nomeArquivo = arquivos[i].name.substr(0, 25) + "...";
            }
            tableArquivos += "<tr><td>"+nomeArquivo+"</td><td>"+formatFileSize(arquivos[i].size)+"</td></tr>";
        }
        tableArquivos += "</table>";

        $('#arquivosUpload').tooltipster({
            side: 'right',
            theme: 'tooltipster-borderless',
            delay: 0,
            animation: 'grow',
            trigger: 'click',
            contentAsHTML: true
        });
        $('#arquivosUpload').tooltipster('content', tableArquivos);
    });

    $("#add-mail-friend").click(function(){
        var empty = 0;
        $(".email-amigo").each(function(){
            if($(this).val() == ""){
                empty++;
            }
        });

        if(empty == 0){
            $("#lista-email-amigos").append("<input type=\"email\" name=\"emails-amigos[]\" class=\"campo-form email-amigo email-amigo-adicionado\" placeholder=\"E-mail do amigo\">");
            $(".email-amigo-adicionado:last-child").focus();
        }
    });

    $(".controls-form").delegate('.email-amigo-adicionado', 'focusout', function(event) {
        if($(this).val() == "") {
            $(this).remove();
        }
    });
});