(function() {
	$("#form-upload").ajaxForm({
	    beforeSend: function() {
	    	var arquivos = listaArquivos();
	    	if(arquivos.length > 0){
		    	var percentVal = '0%';
		        $('.pie_progress').asPieProgress('reset');
		        $("#loading").fadeIn("fast");
	    	}
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        $('.pie_progress').asPieProgress('go', percentVal);
	    },
	    success: function() {
			var percentVal = '100%';
	        $('.pie_progress').asPieProgress('go', percentVal);
	    },
		complete: function(response) {
			console.log(response);
			$("#compartilhar").html(response.responseText);
			uploadComplete();
		}
	}); 

})(); 