<?php
class Conexao {
	private $servidor = "localhost";
	private $usuario = "evoke436_yobib";
	private $senha = "";
	private $nomedobanco = "evoke436_yobib";
	private $conexao = NULL;
	
	public function __construct() {	
		$this->conecta();
	}
	
	public function __destruct() {
		if ($this->conexao != NULL) {
			mysqli_close($this->conexao);
		}
	}
	
	public function conecta() {
		$this->conexao = mysqli_connect($this->servidor, $this->usuario, $this->senha, $this->nomedobanco);
		
		mysqli_query($this->conexao, "SET NAMES 'utf8'");
		mysqli_query($this->conexao, "SET CHARACTER SET 'utf8'");
		mysqli_query($this->conexao, "SET character_set_results=utf8");
		mysqli_query($this->conexao, "SET character_set_connection=utf8");
		mysqli_query($this->conexao, "SET character_set_client=utf8");
		mysqli_query($this->conexao, "SET character_set_results=utf8");	
	}
	
	public function getConexao() {
		return $this->conexao;
	}
	
}