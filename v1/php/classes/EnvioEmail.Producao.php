<?php

require_once (dirname ( __FILE__ ) . "/../../resources/php-mailer/PHPMailerAutoload.php");

class EnvioEmail {
	
	//Pra essa classe funcionar no hostgator	
	//se a aplicação que faz o envio esta no ambiente de hospedagem deve colocar "localhost" e usar a porta 26.
	//se a aplicação esta fora do ambiente de hospedagem deve usar mail.dominio e porta 587
	
	const charSet = "UTF-8";
	const host = "localhost";
	const porta = 587;
	const seguranca = "tls";
	
	const autenticacao = true;
	const usuario = "nao-responda@yobib.com.br";
	const senha = "123!P@ssword";
	
	private $destinatario = array();
	private $copiaOculta = array();
	private $assunto;
	private $mensagem;
	private $from;
	
	public function getDestinatario() {
		return $this->destinatario;
	}
	public function adicionaDestinatario($destinatario) {
		array_push($this->destinatario, $destinatario);
		return $this;
	}
	public function getCopiaOculta() {
		return $this->copiaOculta;
	}
	public function adicionaCopiaOculta($copiaOculta) {
		array_push($this->copiaOculta, $copiaOculta);
		return $this;
	}
	public function getAssunto() {
		return $this->assunto;
	}
	public function setAssunto($assunto) {
		$this->assunto = $assunto;
		return $this;
	}
	public function getMensagem() {
		return $this->mensagem;
	}
	public function setMensagem($mensagem) {
		$this->mensagem = utf8_decode($mensagem);
		return $this;
	}
	
	public function getFrom() {
		return $this->from;
	}
	public function setFrom($from) {
		$this->from = $from;
		return $this;
	}

	public function getUsuario() {
		return self::usuario;
	}
		
	function enviaEmail() {
		$mail = new PHPMailer();
		$mail->charSet = self::charSet;
		$mail->isSMTP();
		$mail->isHTML(true);
		$mail->Host = self::host;
		$mail->Port = self::porta;
		
		$mail->SMTPSecure = self::seguranca;
		$mail->SMTPAuth = self::autenticacao;
		
		$mail->Username = self::usuario;
		$mail->Password = self::senha;
		
		$mail->setFrom($this->getFrom(), "Yobib."); // quem ta enviando o e-mail
		$mail->Subject = utf8_decode($this->assunto);				
		$mail->msgHTML($this->mensagem);
		
		for($i = 0; $i < count($this->getDestinatario()); $i++){
			$destinatarios = $this->getDestinatario();			
			$mail->addAddress($destinatarios[$i]);
		}
		
		for($i = 0; $i < count($this->getCopiaOculta()); $i++){
			$copias = $this->getCopiaOculta();
			$mail->addBCC($copias[$i]);
		}
		
		// email em texto puro
		//$mail->AltBody = $this->mensagem;
		
		// anexar arquivos
		// $mail->addAttachment($path)
		
		if ($mail->send()) {
			//echo "E-mail enviado com sucesso <br>";
			return true;
		} else {
			echo $mail->ErrorInfo;
		}
	}
}