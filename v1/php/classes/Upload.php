<?php 
	class Upload {
		private $id;
		private $md5Folder;
		private $proprietario;
		private $emailsAmigos = array();
		private $mensagem;
		private $dataUpload;
		private $dataExcluido;
		private $excluido;

		function __construct($md5Folder, $dataUpload) {
			$this->setMd5Folder($md5Folder);
			$this->setDataUpload($dataUpload);
			$dataExcluido = date('Y-m-d', strtotime(date("Y-m-d"). '+2 days')).date(" H:i:s");
			$this->setDataExcluido($dataExcluido);
		}

		function setId($id) {
			$this->id = $id;
		}
		function getId() {
			return $this->id;
		}

		function setMd5Folder($md5) {
			$this->md5Folder = $md5;
		}
		function getMd5Folder() {
			return $this->md5Folder;
		}

		function setProprietario($email) {
			$this->proprietario = $email;
		}
		function getProprietario() {
			return $this->proprietario;
		}

		function setDataUpload($data) {
			$this->dataUpload = $data;
		}
		function getDataUpload() {
			return $this->dataUpload;
		}

		function setDataExcluido($data) {
			$this->dataExcluido = $data;
		}
		function getDataExcluido() {
			return $this->dataExcluido;
		}

		function setExcluido($bool) {
			$this->excluido = $bool;
		}
		function getExcluido() {
			return $this->excluido;
		}

		function setMensagem($mensagem) {
			$this->mensagem = $mensagem;
		}
		function getMensagem(){
			return $this->mensagem;
		}

		function addEmailAmigo($email) {
			array_push($this->emailsAmigos, $email);
		}
		function setEmailsAmigos($emails) {
			foreach ($emails as $key => $email) {
				$this->addEmailAmigo(trim($email));
			}
		}
		function getEmailsAmigos() {
			return $this->emailsAmigos;
		}

		function formataData($data) {
			$data = date_create($data);
			return date_format($data, "d/m/Y H:i:s");
		}

		function formatFileSize($size) {
		    $units = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		    $power = $size > 0 ? floor(log($size, 1024)) : 0;
		    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
		}

		function deleteDir() {
			$dir = "../../u/".$this->getMd5Folder()."/";

			if(is_dir($dir)){
				$files = array_diff(scandir($dir), array('.', '..')); 

			    foreach ($files as $file) { 
			        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
			    }

			    if(rmdir($dir)){
					return true;
			    }	
			} 

			return false;
		} 

		function enviaEmail(){
			$link = "http://www.yobib.com.br/u/{$this->getMd5Folder()}";
			
			$mensagem = "<html>
						<head>
							<meta charset='UTF-8'>
							<title>yobib.</title>
							<meta name='viewport' content='width=device-width, initial-scale=1.0' />
							<style>										
							</style>				
						</head>
						<body>
							<div style='border-radius: 15px; border: solid 1px #4f4f4f; width: 70%; margin: 0 auto'>
								<div style='margin-bottom: 25px;'>
									<table style='width: 100%; border-collapse: collapse; border-radius: 12px 12px 0 0; background-color: #4f4f4f;'>
										<tr>
											<td style='width: 100%; text-align: center'><a href='http://www.yobib.com.br/' target='_blank'><img style='width: 230px; padding: 3px 0 ;height: auto' src='http://www.yobib.com.br/images/logo_yobib.png' alt='yobib.'></td>
										</tr>
									</table>
								</div>
								<div style='clear: both'></div>
								<div style='margin: 20px 0; padding: 0 40px; height: auto'>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #4f4f4f'>Olá!</p>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #4f4f4f; text-align: justify'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #4f4f4f; text-align: justify'>
										O e-mail <strong>{$this->getProprietario()}</strong> compartilhou arquivos com você.
									</p>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #4f4f4f; text-align: justify'>
										<strong>{$this->getMensagem()}</strong>
									</p>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #4f4f4f; text-align: justify'>
										Os arquivos ficarão disponíveis até <strong>".$this->formataData($this->getDataExcluido())."</strong>
										<br>Para visualizar os arquivos clique no link abaixo:
									</p>
								</div>
								<div style='margin: 0px 40px 30px; width: 70%; height: auto'>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #4f4f4f'><a href='{$link}' target='_blank'>{$link}</a></p>
								</div>
							</div>
						</body>
					</html>";

			$mail = new EnvioEmail();
			$mail->setFrom($this->getProprietario());
			$mail->setAssunto("Compartilhamento de arquivos - Yobib.");
			$mail->setMensagem($mensagem);

			$lista = $this->getEmailsAmigos();
			foreach ($lista as $key => $email) {
				$mail->adicionaDestinatario($email);
			}
			
			// $mail->adicionaCopiaOculta("adrianomfpassos@gmail.com");
			$mail->enviaEmail();
		}
	}
?>