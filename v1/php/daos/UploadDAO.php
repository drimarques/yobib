<?php 
	class UploadDAO {
		private $conexao;

		public function __construct() {
			$this->conexao = new Conexao();
		}

		public function registraUpload(Upload $up){
			try {
				$query = "INSERT INTO uploads (md5_folder, proprietario, data_upload) VALUES ('{$up->getMd5Folder()}', '{$up->getProprietario()}', '{$up->getDataUpload()}')";
				if(mysqli_query($this->conexao->getConexao(), $query)) {
					return true;
				}	
			} catch (Exception $e) {
				echo $ex->getMessage();
				return false;
			}
		}

		public function update(Upload $up){
			try {
				$query = "UPDATE uploads SET data_excluido = '".date("Y-m-d H:i:s")."', excluido = '1' WHERE id = '{$up->getId()}'";
				if(mysqli_query($this->conexao->getConexao(), $query)) {
					return true;
				}	
			} catch (Exception $e) {
				echo $ex->getMessage();
				return false;
			}
		}

		public function buscaUpload($campo, $valor){
			try {
				$query = "SELECT * FROM uploads WHERE {$campo} = '{$valor}' ORDER BY id ASC";
				$retorno = mysqli_query($this->conexao->getConexao(), $query);
				$linha = mysqli_fetch_array($retorno);
				
				$upload = new Upload($linha["md5_folder"], $linha["data_upload"]);
				$upload->setId($linha["id"]);
				$upload->setProprietario($linha["proprietario"]);
				$upload->setDataExcluido($linha["data_excluido"]);
				$upload->setExcluido($linha["excluido"]);

				return $upload;
				
			} catch (Exception $e) {
				echo $ex->getMessage();
				return false;
			}
		}

		public function buscaUploadsExcluir($dataLimite){
			try {
				$query = "SELECT * FROM uploads WHERE data_upload <= '{$dataLimite}' AND excluido = 0 ORDER BY id ASC";
				$retorno = mysqli_query($this->conexao->getConexao(), $query);
				
				$array = array();
				while($linhas = mysqli_fetch_array($retorno)){
					$upload = new Upload($linhas["md5_folder"], $linhas["proprietario"], $linhas["data_upload"]);
					$upload->setId($linhas["id"]);
					$upload->setDataExcluido($linhas["data_excluido"]);
					$upload->setExcluido($linhas["excluido"]);

					array_push($array, $upload);
				}
				
				$arrayObjecy = new ArrayObject($array);
				return $arrayObjecy;
				
			} catch (Exception $e) {
				echo $ex->getMessage();
				return false;
			}	
		}
	}
?>