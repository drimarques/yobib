<?php
date_default_timezone_set("Brazil/East");

function carregaClasse($arquivo){
	if(file_exists("../classes/".$arquivo.".php")){
		require_once("../classes/".$arquivo.".php");
	} 
	elseif(file_exists("../daos/".$arquivo.".php")){
		require_once("../daos/".$arquivo.".php");
	}
}
spl_autoload_register("carregaClasse");

$emailProprietario = $_POST["email"];
$emailsAmigos = $_POST["emails-amigos"];
$mensagem = $_POST["mensagem"];

$md5Upload = substr(md5(uniqid(rand(1, 6))), 0, 10);
$diretorio = "../../u/".$md5Upload."/";
$qtdArquivos = count($_FILES["arquivos"]["name"]);
$qtdArquivosPasta = count($_FILES["pasta"]["name"]);

if($qtdArquivos > 0 || $qtdArquivosPasta > 0) {
	$old = umask(0);
	if(mkdir($diretorio, 0755)) {

// 		//Cria arquivo index
		$arquivoIndex = fopen($diretorio."index.php", "w");
		fwrite($arquivoIndex, '<?php require_once "../../php/servicos/ListaArquivosUpload.php"; ?>');
		chmod($diretorio."index.php", 0644);
		fclose($arquivoIndex);

		$retorno = "<h2>Compartilhar:</h2>
		        	<form>
		        		<input type='text' name='link-compartilhar' id='link-compartilhar' value='http://www.yobib.com.br/u/{$md5Upload}'>
		        		<input type='button' name='btn-copiar-link' id='btn-copiar-link' value='Copiar link' class='botao'>
		        	</form>

		        	<div id='arquivos-success'>
			        	<ul>";

		for ($i = 0; $i < $qtdArquivos; $i++) {
			$dirUpload = $diretorio.$_FILES['arquivos']['name'][$i];
			if(move_uploaded_file($_FILES['arquivos']['tmp_name'][$i], $dirUpload)) {
				$retorno .= "<li>".substr($_FILES['arquivos']['name'][$i], 0 , 32)." <img class='tooltip' title='Arquivo enviado com sucesso' src=\"images/upload-success.svg\"></li>";
			} else {
				$retorno .= "<li>".substr($_FILES['arquivos']['name'][$i], 0 , 32)." <img class='tooltip' title='Erro ao enviar o arquivo' src=\"images/upload-error.svg\"></li>";
			}
		}

		for ($j = 0; $j < $qtdArquivosPasta; $j++) {
			$dirUpload = $diretorio.$_FILES['pasta']['name'][$j];
			if(move_uploaded_file($_FILES['pasta']['tmp_name'][$j], $dirUpload)) {
				$retorno .= "<li>".substr($_FILES['pasta']['name'][$j], 0 , 32)." <img class='tooltip' title='Arquivo enviado com sucesso' src=\"images/upload-success.svg\"></li>";
			} else {
				$retorno .= "<li>".substr($_FILES['pasta']['name'][$j], 0 , 32)." <img class='tooltip' title='Erro ao enviar o arquivo' src=\"images/upload-error.svg\"></li>";
			}
		}


		$upload = new Upload($md5Upload, date("Y-m-d H:i:s"));

		$emailProprietario = (isset($emailProprietario)) ? $emailProprietario : null;
		if($emailProprietario != null) {
			$upload->setProprietario($emailProprietario);
			$upload->setEmailsAmigos($emailsAmigos);
			$upload->setMensagem($mensagem);
			$upload->enviaEmail();
		}

		$dao = new UploadDAO();
		$dao->registraUpload($upload);

		$retorno .= "</ul>
					</div>";
		echo $retorno;
	}
}