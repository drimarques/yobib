<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>yobib.</title>
	<link rel="shortcut icon" href="../../images/icone_yobib.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="../../resources/porgressbar/progress.css" rel="stylesheet" type="text/css" />
	<link href="../../resources/tooltipser/tooltipster.bundle.css" rel="stylesheet" type="text/css" />
	<link href="../../resources/tooltipser/tooltipster-sideTip-borderless.min.css" rel="stylesheet" type="text/css" />
	<!-- build:css ../../css/index.min.css -->
	<link href="../../css/index.css" rel="stylesheet" type="text/css" />
	<!-- endbuild -->

	<meta name="description" content="" />
	<meta name="keywords" content="" />
</head>
<body>
	<main>
		<header id="cabecalho">
			<nav>
				<ul>
					<li><a href="."><img src="../../images/logo_yobib.svg" alt="yobib"></a></li>
				</ul>
			</nav>
		</header>

		<article id="central">
			<h1>Faça o download dos seus arquivos, exemplo exemplo exemplo...</h1>

			<ul id="acoes-arquivos">
				<li id="select-all">Marcar todos <img src="../../images/check.svg"></li>
				<li id="download" class="tooltip2" title="Selecione um arquivo para download">Download <img src="../../images/download.svg"></li>
			</ul>

			<table class="tabela" id="arquivos-uploaded">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Tamanho do arquivo</th>
						<th>Proprietário</th>
						<th>Data do envio</th>
					</tr>
				</thead>
				<tbody>
					<?php
						require_once "../../php/servicos/CarregaArquivos.php";
					?>
		</article>
	</main>
	<script src="../../resources/jquery/jquery-2.2.3.min.js"></script>
	<script src="../../resources/jquery/jquery.easing.min.js"></script>
	<script src="../../resources/porgressbar/jquery-asPieProgress.js"></script>
	<script src="../../resources/jquery/jquery.form.js"></script>
	<script src="../../resources/tooltipser/tooltipster.bundle.js"></script>
	<script src="../../js/index.js"></script>
</body>
</html>