function formatFileSize(bytes, decimals) {
   if(bytes == 0) return '0 bytes';
   var k = 1000;
   var dm = decimals + 1 || 3;
   var sizes = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
}

var notificacao = function(texto = null){
    $("#notificacao span").text(texto);
    $("#notificacao").slideToggle("fast");

    setTimeout(function(){
        $("#notificacao").slideToggle("fast");
    }, 2000);
}

var uploadComplete = function() {
    $("#status-upload").text("Upload completo!");
    $("#compartilhar").fadeIn("fast");
    $("#arquivos-success").fadeIn("fast");
};

var listaArquivos = function(){
    var lista = new Array();

    var arquivos = $("#arquivos")[0].files;
    for (var i = 0; i < arquivos.length; i++) {
        lista.push(arquivos[i]);
    }

    var arquivosPasta = $("#pasta")[0].files;
    for (var i = 0; i < arquivosPasta.length; i++) {
        lista.push(arquivosPasta[i]);
    }

    return lista;
}

$(function(){ 
	$('.pie_progress').asPieProgress({
        namespace: 'pie_progress'
    });

    // $('.pie_progress').asPieProgress('go', "100");

    $("#section-percent-upload").delegate("#btn-copiar-link" ,"click", function() {
        var copyTextarea = document.getElementById("link-compartilhar");
        copyTextarea.select();

        try {
            var successful = document.execCommand('copy');
            if(successful){
                notificacao("Link copiado");
            }           
        } catch (err) {
            
        }
    });
});