$(function(){
	$("#pasta").change(function() {
		$("#arquivos").removeAttr('required');
	});

	$("#arquivos").change(function() {
		$("#pasta").removeAttr('required');
	});

	$("#email").focusout(function() {
		if($(this).val() != ""){
			$("#email-amigo").attr("required", "required");
		} else {
			$("#email-amigo").removeAttr('required');
		}
	});


	$("#email-amigo").focusout(function() {
		if($(this).val() != ""){
			$("#email").attr("required", "required");
		} else {
			$("#email").removeAttr('required');
		}
	});

    $("#add-mail-friend").click(function(){
        var empty = 0;

        if($("#email-amigo").val() == ""){
            empty++;
        }
        
        $(".email-amigo").each(function(){
            if($(this).val() == ""){
                empty++;
            }
        });

        if(empty == 0){
            $("#lista-email-amigos").append("<input type=\"email\" name=\"emails-amigos[]\" class=\"campo-form email-amigo email-amigo-adicionado\" placeholder=\"outro amigo?\">");
            $(".email-amigo-adicionado:last-child").focus();
        }
    });

    $(".form-control").delegate('.email-amigo-adicionado', 'focusout', function(event) {
        if($(this).val() == "") {
            $(this).remove();
        }
    });
});