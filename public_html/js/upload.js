(function() {
	$("#form-upload").ajaxForm({
	    beforeSend: function() {
	    	var arquivos = listaArquivos();
	    	if(arquivos.length > 0){
	    		$("#section-archive").css({"display": "none"});
	    		$("#section-percent-upload").fadeIn("fast");
		    	
		    	var percentVal = '0%';
		        $('.pie_progress').asPieProgress('reset');
	    	}
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        $('.pie_progress').asPieProgress('go', percentVal);
	    },
	    success: function() {
			var percentVal = '100%';
	        $('.pie_progress').asPieProgress('go', percentVal);
	    },
		complete: function(response) {
			console.log(response);
			$("#compartilhar").html(response.responseText);
			uploadComplete();
		}
	}); 

})(); 
