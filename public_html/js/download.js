$(function(){
// Download
    $("#arquivos-uploaded tbody tr").click(function(){
        var tds = $(this).children("td");
        var td = tds[0];
        var check = $(td).children();

        if($(check).is(":checked")){
            console.log("checked");
            $(check).prop('checked', false);
        } else {
            console.log("NO checked");
            $(check).prop('checked', true);
        }
    })

    $(".check-download").click(function(e) {
        //Quando clicar só no check
        e.stopPropagation();
    });
    
    $(".central-download").delegate('#select-all', 'click', function() {
        var arquivos = $(".check-download");
        var label = $(this).parent("label");

        if($(this).is(":checked")){
            $(label).html("<input type=\"checkbox\" name=\"select-all\" value=\"select-all\" id=\"select-all\" checked=\"checked\">Desmarcar todos os arquivos");

            $.each(arquivos, function(index, arquivo) {
                 $(arquivo).prop('checked', true);
            });

        } else {
            
            $(label).html("<input type=\"checkbox\" name=\"select-all\" value=\"select-all\" id=\"select-all\">Marcar todos os arquivos");

            $.each(arquivos, function(index, arquivo) {
                 $(arquivo).prop('checked', false);
            });
        }
    })

    $("#download").click(function(e){
        e.preventDefault();

        var selecionados = $(".check-download:checked");

        if (selecionados.length > 0) {
            for (var i = 0; i < selecionados.length; i++) {
                var idDownload = $(selecionados[i]).attr('id') + "download";
                document.getElementById(idDownload).click();
            }
        } else {
            notificacao("Selecione um arquivo");
        }
    });
});