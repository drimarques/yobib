<?php
	session_start();
	date_default_timezone_set('Brazil/East');
	require_once "php/servicos/RegistraContagemAcessos.php";
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WRMCJR');</script>
	<!-- End Google Tag Manager -->

	<meta charset="UTF-8">
	<title>yobib.</title>
	<link rel="shortcut icon" href="images/yobib-ico.png" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="resources/porgressbar/progress.css" rel="stylesheet" type="text/css" />
	
	<!-- build:css css/index.min.css -->
	<link href="css/index.css" rel="stylesheet" type="text/css" />
	<!-- endbuild -->

	<meta name="application-name" content="Yobib"/> 
	<meta name="autor" content="Adriano Marques"/> 
	<meta name="description" content="" />
	<meta name="keywords" content="yobib, Yobib, YOBIB, upload de arquivos, cloud, arquivos, armazenamento, upload, transferência, download, drive, nuvem, arquivos em nuvem" />

	<script type="application/ld+json">
	{
	"@context" : "http://schema.org",
	"@type" : "SoftwareApplication",
	"name" : "Yobib",
	"image" : "http://yobib.com.br/images/logotipo-branco.png",
	"url" : "http://yobib.com.br/",
	"applicationCategory" : "internet",
	"softwareVersion" : "2.0"
	}
	</script>
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRMCJR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<main>
		<header id="cabecalho">
			<nav>
				<ul>
					<li><a href="."><img src="images/logotipo-branco.png" alt="yobib"></a></li>
				</ul>
			</nav>
		</header>

		<article id="central">
			<?php 
				if (isset($_SESSION["arquivo_removido"])) {
					echo "<div id='arquivo-removido'>
							<h1>Seu arquivo foi removido :(</h1>
							<a href=".">Fazer outro upload</a>
						</div>";
					unset($_SESSION["arquivo_removido"]);
					die();
				}
			?>

			<section id="principal">
				<div>
					<form method="post" action="php/servicos/RealizaUpload.php" name="form-upload" id="form-upload" class="formulario" enctype="multipart/form-data" autocomplete="off">
						<div class="section-formulario">

							<div id="section-percent-upload">
								<h1 id="status-upload">Fazendo upload...</h1>

								<div class="pie_progress" role="progressbar" data-goal="100" aria-valuemin="0" aria-valuemax="100">
						            <span class="pie_progress__number">0%</span>
						        </div>
						        <article id="compartilhar">
						        	<!--Gerado no js-->
						        </article>
							</div>


							<div id="section-archive">
								<h1>Adicione seus arquivos <span>aqui</span></h1>

								<ul>
									<li>
										<div class="botao-upload" id="botao-arquivo">
											<img src="images/icone-arquivo.png" alt="Upload de arquivos">
							    			<span>Arquivos</span>
							    			<input type="file" name="arquivos[]" multiple="multiple" class="input-upload" id="arquivos" required="required">
										</div>
									</li>
									<li>ou</li>
									<li>
										<div class="botao-upload" id="botao-pasta">
											<img src="images/icone-pasta.png" alt="Upload de pasta">
							    			<span>Pastas</span>
							    			<input type="file" name="pasta[]" multiple="multiple" class="input-upload" id="pasta" required="required" webkitdirectory directory>
										</div>
									</li>
								</ul>
							</div>
						</div>

						
						<div class="section-formulario">
							<div id="section-inputs">
								<div class="form-control">
									<input type="email" name="email-amigo" class="campo-form" id="email-amigo" placeholder="E-mail do amigo">
								</div>

								<div class="form-control" id="lista-email-amigos">
									<img src="images/icone-add.png" alt="Adicionar mais amigos" id="add-mail-friend" title="Adicione mais amigos">
									<input type="email" name="emails-amigos[]" class="campo-form email-amigo" placeholder="outro amigo?">
								</div>

								<div class="form-control">
									<input type="email" name="email" class="campo-form" id="email" placeholder="Seu e-mail">
								</div>

								<div class="form-control">
									<textarea name="mensagem" class="campo-form" placeholder="Mensagem" id="mensagem"></textarea>
								</div>

								<div class="form-control">
									<input type="submit" name="btn-enviar" id="btn-enviar" class="botao" value="Enviar">
								</div>
							</div>
						</form>
				</div>
			</section>
		</article>

		<section id="notificacao">
			<span></span>
		</section>

		<footer>
			<div id="termos"><a href="#">Termos de uso e privacidade</a></div>
			<div id="copy">&copy<?php echo date("Y")?> - yobib. Todos os direitos reservados.</div>
			<div id="developer">Desenvolvido por <a href="https://www.linkedin.com/in/adriano-marques-04b522b6" target="_blank">Adriano Marques</a></div>
		</footer>
	</main>
	<script src="resources/jquery/jquery-2.2.3.min.js"></script>
	<script src="resources/jquery/jquery.easing.min.js"></script>
	<script src="resources/porgressbar/jquery-asPieProgress.js"></script>
	<script src="resources/jquery/jquery.form.js"></script>
	<script src="resources/tooltipser/tooltipster.bundle.js"></script>
	<script src="js/index.js"></script>
	<script src="js/formulario.js"></script>

	<!-- build:js js/upload.min.js -->
	<script src="js/upload.js"></script>
	<!-- endbuild -->
</body>
</html>