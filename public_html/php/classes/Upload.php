<?php 
	class Upload {
		private $id;
		private $md5Folder;
		private $proprietario;
		private $emailsAmigos = array();
		private $mensagem;
		private $dataUpload;
		private $dataExcluido;
		private $excluido;

		function __construct($md5Folder, $dataUpload) {
			$this->setMd5Folder($md5Folder);
			$this->setDataUpload($dataUpload);
			$dataExcluido = date('Y-m-d', strtotime(date("Y-m-d"). '+2 days')).date(" H:i:s");
			$this->setDataExcluido($dataExcluido);
		}

		function setId($id) {
			$this->id = $id;
		}
		function getId() {
			return $this->id;
		}

		function setMd5Folder($md5) {
			$this->md5Folder = $md5;
		}
		function getMd5Folder() {
			return $this->md5Folder;
		}

		function setProprietario($email) {
			$this->proprietario = $email;
		}
		function getProprietario() {
			return $this->proprietario;
		}

		function setDataUpload($data) {
			$this->dataUpload = $data;
		}
		function getDataUpload() {
			return $this->dataUpload;
		}

		function setDataExcluido($data) {
			$this->dataExcluido = $data;
		}
		function getDataExcluido() {
			return $this->dataExcluido;
		}

		function setExcluido($bool) {
			$this->excluido = $bool;
		}
		function getExcluido() {
			return $this->excluido;
		}

		function setMensagem($mensagem) {
			$this->mensagem = $mensagem;
		}
		function getMensagem(){
			return $this->mensagem;
		}

		function addEmailAmigo($email) {
			array_push($this->emailsAmigos, $email);
		}
		function setEmailsAmigos($emails) {
			foreach ($emails as $key => $email) {
				$this->addEmailAmigo(trim($email));
			}
		}
		function getEmailsAmigos() {
			return $this->emailsAmigos;
		}

		function formataData($data) {
			$data = date_create($data);
			return date_format($data, "d/m/Y H:i:s");
		}

		function formatFileSize($size) {
		    $units = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		    $power = $size > 0 ? floor(log($size, 1024)) : 0;
		    return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
		}

		function deleteDir() {
			$dir = "../../u/".$this->getMd5Folder()."/";

			if(is_dir($dir)){
				$files = array_diff(scandir($dir), array('.', '..')); 

			    foreach ($files as $file) { 
			        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
			    }

			    if(rmdir($dir)){
					return true;
			    }	
			} 

			return false;
		} 

		function enviaEmail(){
			$link = "http://www.yobib.com.br/u/{$this->getMd5Folder()}";
			
			$mensagem = "<html>
						<head>
							<meta charset='UTF-8'>
							<title>yobib.</title>
							<meta name='viewport' content='width=device-width, initial-scale=1.0' />
							<style>										
							</style>				
						</head>
						<body>
							<div style='width: 500px; margin: 0 auto'>
							<!--<div style='border: solid 1px #2b3e51; width: 500px; margin: 0 auto'>-->
								<div style='margin-bottom: 25px;'>
									<table style='width: 100%; border-collapse: collapse;'>
										<tr>
											<td style='width: 100%; text-align: center'>
												<a href='http://www.yobib.com.br/' target='_blank' style='display: block;'>
													<img style='width: 100%; height: auto' src='http://www.yobib.com.br/images/faixa-email.png'>
												</a>
											</td>
										</tr>
									</table>
								</div>
								<div style='clear: both'></div>


								<div style='margin: 20px 0; padding: 0 40px; height: auto'>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #2b3e51; text-align: left'>
										O e-mail <strong>{$this->getProprietario()}</strong> compartilhou alguns arquivos com você.
									</p>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #2b3e51; text-align: left'>
										<strong>Mensagem:</strong> <br>
										<i>{$this->getMensagem()}</i>
									</p>
									<p style='font-size: 15px; font-family: Arial, sans-serif; color: #2b3e51; text-align: left'>
										Os arquivos ficarão disponíveis até o dia <strong>".$this->formataData($this->getDataExcluido())."</strong>
									</p>
								</div>


								<div style='padding-top: 55px; height: 80px; border-bottom: solid 1px #2b3e51; text-align: center; width: 220px; margin: 0 auto; margin-bottom: 30px'>
									<a href='{$link}' target='_blank' 
										style='
											font-size: 16px; 
											font-family: Arial, sans-serif; 
											text-align: justify; 
											text-decoration: none; 
											color: #FFF; 
											background-color: #2b3e51;
											padding: 17px 55px 17px 25px;
											border-radius: 25px;
											background-image: url(\"http://www.yobib.com.br/images/icone-download.png\");
											background-repeat: no-repeat;
											background-position: top 13px right 20px;
											background-size: 22px auto;
									'>Download</a>
								</div>

								<div style='position: relative'>

									<table style='width: 100%; border-collapse: collapse;'>
										<tr>
											<td style='width: 33%; text-align: left'>
												<img style='padding-left: 30px; width: 40px; height: auto' src='http://www.yobib.com.br/images/icone-facebook.png'>
											</td>
											<td style='width: 33%; text-align: center; font-size: 16px; font-family: Arial, sans-serif; color: #2b3e51;'>
												".date("Y")."
											</td>
											<td style='width: 33%; text-align: right'>
												<img style='padding-right: 30px; width: 120px; height: auto' src='http://www.yobib.com.br/images/logotipo-azul.png'>
											</td>
										</tr>
									</table>
									<p style='margin-top: 15px; font-size: 12px; font-family: Arial, sans-serif; color: #2b3e51; text-align: center; width: 100%; '>
										©".date("Y")." - yobib. Todos os direitos reservados.
									</p>
								</div>
							</div>
						</body>
					</html>";

			$mail = new EnvioEmail();
			$mail->setFrom($this->getProprietario());
			$mail->setAssunto("Compartilhamento de Arquivos");
			$mail->setMensagem($mensagem);

			$lista = $this->getEmailsAmigos();
			foreach ($lista as $key => $email) {
				$mail->adicionaDestinatario($email);
			}
			
			$mail->enviaEmail();
		}
	}
?>