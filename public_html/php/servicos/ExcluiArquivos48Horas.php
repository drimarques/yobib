<?php 
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
date_default_timezone_set("Brazil/East");

function carregaClasse($arquivo){
	if(file_exists("../../php/classes/".$arquivo.".php")){
		require_once("../../php/classes/".$arquivo.".php");
	} 
	elseif(file_exists("../../php/daos/".$arquivo.".php")) {
		require_once("../../php/daos/".$arquivo.".php");
	}
}

spl_autoload_register("carregaClasse");

$data_corte = date('Y-m-d', strtotime(date("Y-m-d"). '-2 days')).date(" H:i:s");
$dao = new UploadDAO();
$uploads = $dao->buscaUploadsExcluir($data_corte);

foreach ($uploads as $linha => $upload) {
	if($upload->deleteDir()){
		$dao->update($upload);
	}
	else {
		echo "diretorio não encontrado <br>";	
	}
}

