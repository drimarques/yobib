<?php 

function carregaClasse($arquivo){
	if(file_exists("../../php/classes/".$arquivo.".php")){
		require_once("../../php/classes/".$arquivo.".php");
	} 
	elseif(file_exists("../../php/daos/".$arquivo.".php")){
		require_once("../../php/daos/".$arquivo.".php");
	}
}

spl_autoload_register("carregaClasse");

$diretorios = explode("/", getcwd());
$md5Folder = $diretorios[count($diretorios) - 1];
$linkDownload = null;

$dao = new UploadDAO();
$upload = $dao->buscaUpload("md5_folder", $md5Folder);

if($upload->getMd5Folder() == ""){
	header("Location: ../../index.html");
	die();
}

$dir = "../".$md5Folder."/";
$dir = dir($dir);
$tr = "";
while($arquivo = $dir->read()){
	if($arquivo != "." && $arquivo != ".." && $arquivo != "index.php"){
		$idMd5 = md5($arquivo.filesize($arquivo));
		$tr .= "<tr class='hover'>
					<td><input type=\"checkbox\" name=\"select-archive\" value=\"{$arquivo}\" class='check-download' checked=\"checked\" id='{$idMd5}'></td>
					<td>".substr($arquivo, 0, 23)."</td>
					<td class='mobile-right'>{$upload->formatFileSize(filesize($arquivo))}</td>
					<td class='mobile'><a href='mailto:{$upload->getProprietario()}'>{$upload->getProprietario()}</a></td>
					<td class='mobile'>{$upload->formataData($upload->getDataUpload())}</td>
				</tr>";				

		$linkDownload.= "<a id='{$idMd5}download' style='display: none' href='{$arquivo}' download='{$arquivo}'></a>";
	}
}
$dir->close();
$tr .= "</tbody>
	</table>";
	
echo $tr;
echo $linkDownload;