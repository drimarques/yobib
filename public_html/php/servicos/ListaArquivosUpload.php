<!DOCTYPE html>
<html>
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WRMCJR');</script>
	<!-- End Google Tag Manager -->

	<meta charset="UTF-8">
	<title>yobib.</title>
	<link rel="shortcut icon" href="../../images/yobib-ico.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- build:css ../../css/index.min.css -->
	<link href="../../css/index.css" rel="stylesheet" type="text/css" />
	<!-- endbuild -->

	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<script type="application/ld+json">
	{
	"@context" : "http://schema.org",
	"@type" : "SoftwareApplication",
	"name" : "Yobib",
	"image" : "http://yobib.com.br/images/logotipo-branco.png",
	"url" : "http://yobib.com.br/",
	"applicationCategory" : "internet",
	"softwareVersion" : "2.0"
	}
	</script>
	
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRMCJR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
	<main>
		<header id="cabecalho">
			<nav>
				<ul>
					<li><a href="."><img src="../../images/logotipo-branco.png" alt="yobib"></a></li>
				</ul>
			</nav>
		</header>

		<article id="central" class="central-download">
			<form name="form-download" id='form-download'>
				<button id="download">Download</button>
				<label><input type="checkbox" name="select-all" value="select-all" id="select-all" checked="checked">Desmarcar todos os arquivos</label>
				<span id="compartilhe">Compartilhe esse link com outras pessoas</span>

				<table class="tabela" id="arquivos-uploaded">
					<thead>
						<tr>
							<th></th>
							<th>Nome do arquivo</th>
							<th class='mobile-right'>Tamanho</th>
							<th class='mobile'>Proprietário</th>
							<th class='mobile'>Data de envio</th>
						</tr>
					</thead>
					<tbody>
						<?php
							require_once "../../php/servicos/CarregaArquivos.php";
						?>
			</form>
		</article>

		<section id="notificacao">
			<span></span>
		</section>

		<footer>
			<div id="termos"><a href="#">Termos de uso e privacidade</a></div>
			<div id="copy">&copy<?php echo date("Y")?> - yobib. Todos os direitos reservados.</div>
			<div id="developer">Desenvolvido por <a href="https://www.linkedin.com/in/adriano-marques-04b522b6" target="_blank">Adriano Marques</a></div>
		</footer>
	</main>
	<script src="../../resources/jquery/jquery-2.2.3.min.js"></script>
	<script src="../../resources/jquery/jquery.easing.min.js"></script>
	<script src="../../resources/porgressbar/jquery-asPieProgress.js"></script>
	<script src="../../js/index.js"></script>
	<script src="../../js/download.js"></script>
</body>
</html>